<?php

/**
 * @file
 * template.php
 */
function medicom_preprocess_page (&$variables) {
  $alias_parts = explode('/', drupal_get_path_alias());
  $theme_path = drupal_get_path('theme', 'medicom');

  if (count($alias_parts) && $alias_parts[0] == 'front2') {
    // $variables['theme_hook_suggestions'][] = 'page__front2';
  }
  elseif (count($alias_parts) && $alias_parts[0] == 'front3') {
    // $variables['theme_hook_suggestions'][] = 'page__front3';
    // drupal_add_js(drupal_get_path('theme', 'medicom').'/js/video.js');
  }
  elseif (count($alias_parts) && $alias_parts[0] == 'front4') {
    // $variables['theme_hook_suggestions'][] = 'page__front4';
  }
  elseif (count($alias_parts) && ($alias_parts[0] == 'about-us-2' || $alias_parts[0] == 'shortcodes')) {
    // drupal_add_css($theme_path . '/css/piechart-style.css');
    // drupal_add_js($theme_path . '/js/jquery.easypiechart.min.js', array('scope' => 'footer'));
    // drupal_add_js("
    //   jQuery(document).ready(function() {
    //     //pie charts  
    //     jQuery('#pie-charts').waypoint(function(direction) {     
    //       jQuery('.chart').easyPieChart({
    //         easing: 'easeOutBounce',
    //         onStep: function(from, to, percent) {
    //           jQuery(this.el).find('.percent').text(Math.round(percent));
    //         }
    //       });
    //       }, {
    //       offset: function() {
    //         return jQuery.waypoints('viewportHeight') - jQuery(this).height() + 100;
    //       }
    //     });
    //   });      
    // ", array('scope' => 'footer', 'type' => 'inline'));
  }
  elseif($_GET['q'] == 'blog-single-post' || $alias_parts[0] == 'post-by-date' ){
    //$variables['theme_hook_suggestions'][] = 'page__blogs';
  }
  elseif (isset ($variables['node']->type)) {
    if(isset($variables['node']->title)){
      drupal_set_title($variables['node']->title,true);
    }
    if($variables['node']->type == 'blog' || $variables['node']->type == 'product'){
      //$variables['theme_hook_suggestions'][] = 'page__blogs';
      if($variables['node']->type == 'product'){
        drupal_add_js("
          function productQuantity(oBj,param){
              var itemcount= 1;
              if(parseInt(jQuery(oBj).parent().find('input[type=\"text\"]').val()) > 1){
                itemcount= parseInt(jQuery(oBj).parent().find('input[type=\"text\"]').val());
              }
              if(param == -1){
                if(itemcount > 1){
                  itemcount--;  
                }              
              } else if (param == 1) {
                itemcount++;
              }
              jQuery(oBj).parent().find('input[type=\"text\"]').val(itemcount);
            }
          jQuery(function() { 
            jQuery('.node-type-product .tab-horizontal1 .element-invisible').html('Product <span>Info</span>'); 
            jQuery('.node-type-product .tab-horizontal1 .element-invisible').addClass('light bordered'); 
             
            
          });
          ",'inline');
      }
    }
  } elseif($_GET['q'] == 'contact'){
    // $variables['theme_hook_suggestions'][] = 'page__blogs';
    drupal_set_title(t(''),true);
  }
  // 404 redirect
  // $status = drupal_get_http_header("status");  
  // if($status == "404 Not Found") {    
  //   // get the configured 404 error page url :
  //   $not_found_url = variable_get('site_404');
  //   unset($_GET['destination']);
  //   drupal_goto($not_found_url);
  // }

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-md-4"';
  }
  elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-md-8"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-12"';
  }
  if($_GET['q'] == 'cart' || $alias_parts[0] == 'product-categories'){
    drupal_add_js("
            function productQuantity(oBj,param){
              var itemcount= 1;
              if(parseInt(jQuery(oBj).parent().find('input[type=\"text\"]').val()) > 1){
                itemcount= parseInt(jQuery(oBj).parent().find('input[type=\"text\"]').val());
              }
              if(param == -1){
                if(itemcount > 1){
                  itemcount--;  
                }              
              } else if (param == 1) {
                itemcount++;
              }
              jQuery(oBj).parent().find('input[type=\"text\"]').val(itemcount);
            }
          ",'inline');
  }
  drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . drupal_get_path('theme','medicom') . '" });', 'inline');
  $color_setting = 'blue.css';
  $check_allow = theme_get_setting('allowClient') ? theme_get_setting('allowClient') : 0;
  if( isset($_COOKIE["styleColor"]) && $check_allow == 1) {
    $color_setting = $_COOKIE["styleColor"];

  } else {
    if(theme_get_setting('styleColor')){
      $color_setting = theme_get_setting('styleColor').'.css';
    }
  }
  drupal_add_css(drupal_get_path('theme','medicom') . '/css/'.$color_setting);
  if( isset($_COOKIE["footerClass"]) && $check_allow == 1 ) {
    if ($_COOKIE["footerClass"] == 'light') {
      drupal_add_js("jQuery(function() { 
        jQuery('#footer').addClass('light');
        jQuery('#footer').removeClass('dark');
        jQuery('#footer img' ).attr('src',Drupal.settings.basePath + Drupal.settings.pathToTheme + '/images/footer-logo.jpg');
       });",'inline');
    } else {
      drupal_add_js("jQuery(function() { 
        jQuery('#footer').addClass('dark');
        jQuery('#footer').removeClass('light');
        jQuery('#footer img' ).attr('src',Drupal.settings.basePath + Drupal.settings.pathToTheme + '/images/footer-logo-dark.jpg');
       });",'inline');
    }
  } else {
    $class1 = 'light'; 
    $class2= 'dark';
    $img = 'footer-logo.jpg';
    if(theme_get_setting('footerClass')){
      if(theme_get_setting('footerClass') == 'dark'){
        $class1 = 'dark';
        $class2 = 'light';
        $img = 'footer-logo-dark.jpg';
      }
    }
    drupal_add_js("jQuery(function() { 
        jQuery('#footer').addClass('".$class1."');
        jQuery('#footer').removeClass('".$class2."');
        jQuery('#footer img' ).attr('src',Drupal.settings.basePath + Drupal.settings.pathToTheme + '/images/".$img."');
       });",'inline');
  }
}

function medicom_js_alter(&$js) {
  $bootstrap_js_path = drupal_get_path('theme', 'bootstrap') . '/js/bootstrap.js';
  unset($js[$bootstrap_js_path]);
  $isotope_js_path = drupal_get_path('module', 'views_isotope') . '/views_isotope.js';
  if (isset($js[$isotope_js_path])) {    
    unset($js[$isotope_js_path]);
    drupal_add_js(drupal_get_path('theme', 'medicom') . '/js/views_isotope.js');
  }  
}

function medicom_theme() {
  return array(
    // 'contact_site_form' => array(
    //   'render element' => 'form',
    //   'template' => 'contact-site-form',
    //   'path' => drupal_get_path('theme', 'medicom').'/templates/blocks',
    // ),
    'appointment_node_form' => array(
      'render element' => 'form',
      'template' => 'appointment-node-form',
      // this will set to module/theme path by default:
      'path' => drupal_get_path('theme', 'medicom').'/templates/node',
    ),
  );
}

function medicom_preprocess_contact_site_form(&$vars) {
  $vars['contact'] = drupal_render_children($vars['form']);
}

function medicom_menu_tree__primary(&$variables) {
 return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}

function medicom_date_popup($vars) {
  $element = $vars['element'];
  $attributes = !empty($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array('class' => array());  
  // If there is no description, the floating date elements need some extra padding below them.
  $wrapper_attributes = array('class' => array('date-padding'));
  if (empty($element['date']['#description'])) {
    $wrapper_attributes['class'][] = 'clearfix';
  }
  // Add an wrapper to mimic the way a single value field works, for ease in using #states.
  if (isset($element['#children'])) {
    $element['#children'] = '<div id="' . $element['#id'] . '" ' . drupal_attributes($wrapper_attributes) .'>' . $element['#children'] . '</div>';
  }
  return '<div ' . drupal_attributes($attributes) .'>' . theme('form_element', $element) . '</div>';
}

function medicom_preprocess_html(&$variables) {
  $path = drupal_get_path_alias();
  if(in_array($path, array('gallery','gallery2','gallery3','portfolio','portfolio2','portfolio3'))){
    drupal_add_css(drupal_get_path('theme','medicom').'/css/reset.css') ;  
    if($path  == 'gallery' || $path  == 'gallery2' || $path  == 'gallery3'){
      $variables['head_title'] = 'Gallery '.$variables['head_title'];
    } else {
      $variables['head_title'] = 'Portfolio '.$variables['head_title'];
    }
  }
}
function medicom_form_search_block_form_alter(&$form, &$form_state, $form_id) { 
  $form['search_block_form']['#attributes']['placeholder'] = t('Search.....');
  $form['#prefix'] = '<div class="sidebar-widget">';
  $form['#suffix'] = '</div>';
  drupal_add_js("jQuery(function() { jQuery('.col-lf .form-search button.btn-default').html('<i class=\"fa fa-search\"></i>'); });",'inline');
}
function medicom_form_alter(&$form, &$form_state, $form_id) { 
  if($form_id == 'comment_node_blog_form'){
    $form['comment_body'][LANGUAGE_NONE][0]['#format'] = 'plain_text';
  }
  if($form_id == 'contact_site_form'){
    //print_r($form);die;
    $form['name']['#attributes']["placeholder"] = $form['name']['#title'];
    $form['name']['#title'] = t('');
    $form['mail']['#attributes']["placeholder"] = $form['mail']['#title'];
    $form['mail']['#title'] = t('');
    $form['subject']['#attributes']["placeholder"] = $form['subject']['#title'];
    $form['subject']['#title'] = t('');
    $form['message']['#attributes']["placeholder"] = $form['message']['#title'];
    $form['message']['#title'] = t('');
    if(isset($form['copy'])){
      unset($form['copy']);
    }
  }
  if(substr($form_id, 0, 28) == 'uc_product_add_to_cart_form_' || substr($form_id, 0, 27) == 'uc_catalog_buy_it_now_form_')  {
     // print_r($form);die;
    $form['actions']['submit']['#attributes']['class'][] = 'btn-default pull-left';
  }
}
function medicom_menu_link(array $variables) {
   $element = $variables['element'];
  $sub_menu = '';
  
  if ($element['#below']) {
   
    // Prevent dropdown functions from being added to management menu as to not affect navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    else {
     
      // Add our own wrapper
      unset($element['#below']['#theme_wrappers']);

      if (isset($element['#original_link']['localized_options']['mega']) && $element['#original_link']['localized_options']['mega'] == 1) {
        $sub_menu = '<div class="mega-menu">';
        foreach ($element['#below'] as $key => $data) {
          if (isset($data['#below'])) {
            unset($data['#below']['#theme_wrappers']);
            $sub_menu .= '<ul><li><strong>' . $data['#title'] . '</strong></li>' . drupal_render($data['#below']) . '</ul>';
          }          
        }     
        $sub_menu .= '</div>';
        $element['#attributes']['class'][] = 'mega-menu-item';
        $element['#localized_options']['html'] = TRUE;
      }
      else {
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
       
        // Check if this element is nested within another
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
          // Generate as dropdown submenu
          $element['#attributes']['class'][] = 'dropdown-submenu';
          $sub_menu = '<i class="fa fa-angle-right pull-right"></i>' . $sub_menu;
        }
        else {
          // Generate as standard dropdown
          $element['#attributes']['class'][] = 'dropdown';
          $element['#localized_options']['html'] = TRUE;
          $element['#title'] .= ' <span class="caret"></span>';
        }
       
        // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
        $element['#localized_options']['attributes']['data-target'] = '#';
      }      
    }
  }
  // Issue #1896674 - On primary navigation menu, class 'active' is not set on active menu item.
  // @see http://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
     $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
function medicom_element_info_alter(&$type) {
  if (isset($type['uc_quantity'])) {
    $type['uc_quantity']['#prefix'] = '<div class="item-counter"><span onClick="productQuantity(this,-1);" class="pull-left">-</span>';
    $type['uc_quantity']['#suffix'] = '<span onClick="productQuantity(this,1);" class="pull-right">+</span><div class="clr"></div></div>';
    $type['uc_quantity']['#attributes']['class'][] = 'items-total';
  }
}
function medicom_preprocess_poll_vote(&$variables) { 
  $form = $variables['form'];
  $variables['choice'] = drupal_render($form['choice']);
  $variables['title'] = check_plain($form['#node']->title);
  $variables['vote'] = drupal_render($form['vote']);
  $variables['rest'] = drupal_render_children($form);
  $variables['block'] = $form['#block'];
  $variables['nid'] = $form['#node']->nid;
  if ($variables['block']) {
    $variables['theme_hook_suggestions'][] = 'poll_vote__block';
  }
}
function medicom_preprocess_block(&$vars) {

  $block = $vars['block'];

  if ($block->delta == 'cart') {
    $vars['title_attributes_array']['class'][] = 'bordered light';
  }

}
function medicom_uc_cart_block_title($variables) {
  $title = $variables['title'];
  $icon_class = $variables['icon_class'];
  $collapsible = $variables['collapsible'];
  $collapsed = $variables['collapsed'];
  $arrTitle = explode(' ', $title);
  $i = 0;
  foreach ($arrTitle as $key=>$value) {
    if ($i % 2 == 1) {
        $arrTitle[$key] = '<span>'.$value.'</span>';
    }
    $i++;
  }
  $title = implode(' ', $arrTitle);
  $output = '';

  // Add in the cart image if specified.
  if ($icon_class) {
    $output .= theme('uc_cart_block_title_icon', array('icon_class' => $icon_class));
  }

  // Add the main title span and text, with or without the arrow based on the
  // cart block collapsibility settings.
  if ($collapsible) {
    $output .= '<div class="cart-block-title-bar" title="' . t('Show/hide shopping cart contents.') . '">' . $title;
    if ($collapsed) {
      $output .= '<span class="cart-block-arrow arrow-down"></span>';
    }
    else {
      $output .= '<span class="cart-block-arrow"></span>';
    }
    $output .= '</div>';
  }
  else {
    $output .= '<span class="cart-block-title-bar">' . $title . '</span>';
  }

  return $output;
}
function check_hidden_title($node){
  if(isset($node->type)){
      if(in_array($node->type, array('product','blog','department'))){
        return true;
      }
    }
  return false;
}
function medicom_preprocess_panels_pane(&$vars) {
  $vars['title_attributes_array']['class'][] = 'page-header bordered light';
}

function _extract_select_options ($mytext) {

    $options = $pairs = $temp_array = array();

    // split text into chunks after each line-break;
    // if not visible here, use "backslash n" as explode parameter
    $pairs = explode("\n", $mytext);

    // split each line into pairs of key and value
    foreach($pairs as $pair) {
      $temp_array = explode('|', $pair);
      $options[trim($temp_array[0])] = $temp_array[1];
    }

    return $options;

}
