	jQuery(document).ready(function($) {
		var current = 'blue.css';
		var cookies;

		function readCookie(name,c,C,i){
			if(cookies){ return cookies[name]; }

			c = document.cookie.split('; ');
			cookies = {};

			for(i=c.length-1; i>=0; i--){
			C = c[i].split('=');
			cookies[C[0]] = C[1];
			}

			return cookies[name];
		}
		if (document.cookie.indexOf("styleColor") >= 0) {
			current = readCookie('styleColor');
		} 
		jQuery("#blue" ).click(function(){			  
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "blue.css");
			});  			  
			current = 'blue.css';
			document.cookie="styleColor=" + current;
			//console.log(JSON.stringify(Drupal.settings));
			return false;
		});
		  
		jQuery("#green" ).click(function(){			  
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "green.css");
			}); 		  
			current = 'green.css';
			document.cookie="styleColor=" + current;
			return false;
		});
		  
		jQuery("#red" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "red.css");
			}); 		  
			current = 'red.css';
			document.cookie="styleColor=" + current;
			return false;
		});
		  
		  
		jQuery("#yellow" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "yellow.css");
			}); 		  
			current = 'yellow.css';
			document.cookie="styleColor=" + current;
			return false;
		});

		jQuery("#brown" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "brown.css");
			}); 		  
			current = 'brown.css';
			document.cookie="styleColor=" + current;
			return false;
		});

		jQuery("#cyan" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "cyan.css");
			}); 		  
			current = 'cyan.css';
			document.cookie="styleColor=" + current;
			return false;
		});

		jQuery("#purple" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "purple.css");
			}); 		  
			current = 'purple.css';
			document.cookie="styleColor=" + current;
			return false;
		});

		jQuery("#sky-blue" ).click(function(){
			jQuery('style:contains("'+current+'")').text(function () {
				return jQuery(this).text().replace(current, "sky-blue.css");
			}); 		  
			current = 'sky-blue.css';
			document.cookie="styleColor=" + current;
			return false;
		});		  		  

		// picker buttton
		jQuery(".picker_close").click(function(){

			jQuery("#choose_color").toggleClass("position");

		});
		jQuery("#light").click(function(){
			document.cookie="footerClass=light";
		  	jQuery("#footer").addClass("light");
			jQuery("#footer").removeClass("dark");
			jQuery("#footer img" ).attr("src",Drupal.settings.basePath + Drupal.settings.pathToTheme + "/images/footer-logo.jpg");
		});
		jQuery("#dark").click(function(){
			document.cookie="footerClass=dark";
		  	jQuery("#footer").addClass("dark");
			jQuery("#footer").removeClass("light");
			jQuery("#footer img" ).attr("src",Drupal.settings.basePath + Drupal.settings.pathToTheme +  "/images/footer-logo-dark.jpg");
		});
		   
		  
	});