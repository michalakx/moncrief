jQuery(document).ready(function() {
	//top toggle section
	jQuery(".region-top-sec #toggle-btn").click(function(e) {
    jQuery("#top-sec-detail").slideToggle();
		jQuery("#toggle-btn i").toggleClass("fa-minus");
  });  
  //pie charts  
  jQuery('#pie-charts').waypoint(function(direction) {     
    jQuery('.chart').easyPieChart({
      easing: 'easeOutBounce',
      onStep: function(from, to, percent) {
        jQuery(this.el).find('.percent').text(Math.round(percent));
      }
    });
    }, {
    offset: function() {
      return jQuery.waypoints('viewportHeight') - jQuery(this).height() + 100;
    }
  });

  //
  jQuery('a[data-gal]').each(function() {
		jQuery(this).attr('rel', jQuery(this).data('gal'));
	});  	
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',theme:'light_square',slideshow:false,overlay_gallery: false,social_tools:false,deeplinking:false});
	
	//on click show content
	jQuery('.link-more').click(function(e) {
      jQuery('.content-sec').slideDown(1000);
  });
	//on click hide content
	jQuery('.cross').click(function(e) {
      jQuery('.content-sec').slideUp(1000);
  });

  var file_url = document.getElementById("cus_pdf").getAttribute('href');
  //console.log(file_url);
  var elements = document.getElementsByClassName("embed-link");
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', function (e){
        e.preventDefault();
        var options = {
          pdfOpenParams: {
            navpanes: 1,
           // toolbar: 0,
           // statusbar: 0,
            view: "FitV",
            page: 1,
            zoom: "100%",
            pagemode: "bookmarks"
            
          },
          forcePDFJS: true,
          PDFJS_URL: "/sites/all/libraries/pdf.js/web/viewer.html"
        };

    var myPDF = PDFObject.embed(file_url, "#pdf", options);
    
    var el = document.querySelector("#results");
    el.setAttribute("class", (myPDF) ? "success" : "fail");
    el.innerHTML = (myPDF) ? "Loading.." : "Uh-oh, the embed didn't work.";
    
    setTimeout(function(){
          el.setAttribute("class", "hidden");
          el.innerHTML = "";
    }, 5000);
});
    }
//document.querySelector(".embed-link").addEventListener("click", 

});
