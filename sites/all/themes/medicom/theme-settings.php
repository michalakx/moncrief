<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for medicom themes when admin theme is not.
 *
 * 
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function medicom_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Alter theme settings form.
  $form['medicom'] = array
    (
      '#type' => 'vertical_tabs',
      '#prefix' => '<h2><small>MEDICOM SETTINGS</small></h2>',
      '#weight' => -8
    );
  $form['medicom_settings'] = array
    (
      '#type' => 'fieldset',
      '#title' => 'STYLE SWITCHER',
      '#group' => 'medicom'
    );
  $form['medicom_settings']['header_style']= array(
    '#type' => 'select',
    '#title' => t('Header Style'),
    '#options' => array(
      'header1' => t('Header Style 1'),
      'header2' => t('Header Style 2'),
      'header3' => t('Header Style 3'),
    ),
    '#default_value' => theme_get_setting('header_style')?theme_get_setting('header_style'):'header1',

  );
  $form['medicom_settings']['color_config']= array(
    '#type' => 'fieldset',
    '#title' => 'STYLE SWITCHER',
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,  
    '#prefix' => '<div class="color-switcher" id="choose_color"> ',
    '#suffix' => '</div>'
  );
  $form['medicom_settings']['color_config']['styleColor'] = array(
    '#type' => 'hidden',
    '#title' => 'Color',
    '#default_value' => theme_get_setting('styleColor')?theme_get_setting('styleColor'):'blue',
    '#prefix' => '<div class="theme-colours">
      <p>Choose Colour style</p>
      <ul>
        <li><a href="#" class="blue" onClick="return styleColor(this,\'blue\');"></a></li>
        <li><a href="#" class="green" onClick="return styleColor(this,\'green\');"></a></li>
        <li><a href="#" class="red" onClick="return styleColor(this,\'red\');"></a></li>
        <li><a href="#" class="yellow" onClick="return styleColor(this,\'yellow\');"></a></li>
        <li><a href="#" class="brown" onClick="return styleColor(this,\'brown\');"></a></li>
        <li><a href="#" class="cyan" onClick="return styleColor(this,\'cyan\');"></a></li>
        <li><a href="#" class="purple" onClick="return styleColor(this,\'purple\');"></a></li>
        <li><a href="#" class="sky-blue" onClick="return styleColor(this,\'sky-blue\');"></a></li>
      </ul> ',
    '#suffix' => '</div>'
  );
  $form['medicom_settings']['color_config']['footerClass'] = array(
    '#type' => 'hidden',
    '#title' => 'Footer',
    '#default_value' => theme_get_setting('footerClass')?theme_get_setting('footerClass'):'light',
    '#prefix' => '<div class="choose-footer">
        <p>Choose Footer</p>
        <a href="#" class="light" onClick="return footerClass(this,\'light\');">Light</a>
        <a href="#" class="dark" onClick="return footerClass(this,\'dark\');">Dark</a> ',
    '#suffix' => '</div>'
  );
  $form['medicom_settings']['color_config']['allowClient'] = array(
    '#type' =>'checkbox', 
    '#title' => t('<strong>Allow client switch style on PCs.</strong>'),
    '#default_value' => theme_get_setting('allowClient')?theme_get_setting('allowClient'):0,
  );
  if($color = theme_get_setting('styleColor')){
    drupal_add_js("jQuery(function() { 
      jQuery('.theme-colours .".$color."').addClass('active-color'); 
    });"
    ,'inline');
  } else {
    drupal_add_js("jQuery(function() { 
      jQuery('.theme-colours .blue').addClass('active-color'); 
    });"
    ,'inline');
  }
  if($footer = theme_get_setting('footerClass')){
    drupal_add_js("jQuery(function() { 
      jQuery('.choose-footer .".$footer."').addClass('active-footer'); 
    });"
    ,'inline');
  } else {
    drupal_add_js("jQuery(function() { 
      jQuery('.choose-footer .light').addClass('active-footer'); 
    });"
    ,'inline');
  }
  drupal_add_js("
    function styleColor(oBj, color){
      jQuery('input[name=\"styleColor\"]').val(color);
      jQuery(oBj).parent().parent().find('a').removeClass('active-color');
      jQuery(oBj).addClass('active-color');
      return false;
    }
    function footerClass(oBj, color){
      jQuery('input[name=\"footerClass\"]').val(color);
      jQuery(oBj).parent().parent().find('a').removeClass('active-footer');
      jQuery(oBj).addClass('active-footer');
      return false;
    }"
  ,'inline');
  drupal_add_css('
    .theme-colours ul {
      float: left;
      width: 100%;
    }
    .color-switcher ul li {
      float: left;
      margin-right: 5px;
      margin-bottom: 5px;
      list-style: none;
    }
    .color-switcher ul li a {
      display: block;
      width: 24px;
      height: 24px;
      outline: none;
    }
    .color-switcher ul li a.blue {
      background: #1bb1e9;
    }
    .color-switcher ul li a.green {
      background: #7fc719;
    }
    .color-switcher ul li a.red {
      background: #e54242;
    }
    .color-switcher ul li a.yellow {
      background: #ff9c00;
    }
    .color-switcher ul li a.brown {
      background: #987654;
    }
    .color-switcher ul li a.cyan {
      background: #1abc9c;
    }
    .color-switcher ul li a.purple {
      background: #c74a73;
    }
    .color-switcher ul li a.sky-blue {
      background: #00cdcd;
    }
    .active-color, .color-switcher ul li a:hover{
      border: 3px solid #000;
      width: 18px !important;
      height: 18px !important;
    }
    .active-footer, .choose-footer a:hover {
      border: 3px solid #000 !important;
      text-decoration: none;
      outline: 0;
    }
    .choose-footer a {
      display: inline-block;
      text-align: center;
      width: 50px;
      margin: 0 5px 0 0;
      color: #5d5d5d;
      font-size: 15px;
      border: 3px solid #e7e7e7;
      padding: 5px 7px 2px;
    }
    .color-switcher p {
      font-weight: bold;
    }
    .form-item-allowClient{
      padding-top: 30px !important;
    }
  ','inline');
}
