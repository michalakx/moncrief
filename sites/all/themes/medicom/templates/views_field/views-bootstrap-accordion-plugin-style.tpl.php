<div id="views-bootstrap-accordion-<?php print $id ?>" class="<?php print $classes ?>">
  <?php  $i = 0; foreach ($rows as $key => $row): ?>
  <?php 
    if ($i == 0) {
      $icon_class = 'fa-minus';
      $class = 'active'; 
      $body_status = 'in';
    }
    else {
      $icon_class = '';
      $class = ''; 
      $body_status = ''; 
    }
  ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title <?php print $class; ?>">
          <a class="accordion-toggle"
             data-toggle="collapse"
             data-parent="#views-bootstrap-accordion-<?php print $id ?>"
             href="#collapse<?php print $key ?>">
            <span><i class="fa fa-plus <?php print $icon_class; ?>"></i></span><?php print $titles[$key] ?>
          </a>
        </h4>
      </div>

      <div id="collapse<?php print $key ?>" class="panel-collapse collapse <?php print $body_status; ?>">
        <div class="panel-body">
          <?php print $row ?>
        </div>
      </div>
    </div>
  <?php $i++; endforeach ?>
</div>