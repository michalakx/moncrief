<?php $form['actions']['#children'] = '<input type="submit" value="submit" class="btn btn-default btn-rounded" onClick="validateAppoint();">'; ?>

<?php 
    $form['title']['#attributes']['placeholder'] = $form['title']['#title'];
    unset($form['title']['#title']);
    $form['field_last_name']['und'][0]['value']['#attributes']['placeholder'] = $form['field_last_name']['und'][0]['value']['#title'];
    unset($form['field_last_name']['und'][0]['value']['#title']);
    $form['field_email']['und'][0]['email']['#attributes']['placeholder'] = $form['field_email']['und'][0]['email']['#title'];
    unset($form['field_email']['und'][0]['email']['#title']);
    $form['field_phone']['und'][0]['value']['#attributes']['placeholder'] = $form['field_phone']['und'][0]['value']['#title'];
    unset($form['field_phone']['und'][0]['value']['#title']);
    $form['field_message']['und'][0]['value']['#attributes']['placeholder'] = $form['field_message']['und'][0]['value']['#title'];
    unset($form['field_message']['und'][0]['value']['#title']);
    unset($form['field_date']['und'][0]['value']['date']['#title']);
    unset($form['field_date']['und'][0]['#theme_wrappers']);
    unset($form['field_gender']['und']['#title']);
?>
<div class="row">
    <div class="col-md-6">
        <figure><img src="<?php print drupal_get_path('theme', 'medicom'); ?>/images/appointment-img.jpg" alt="image" title="Appointment image" class="img-responsive lady1"></figure>
    </div>
    <div class="col-md-6">
        <div class="appointment-form clearfix">  
        <form name="appoint_form" id="appoint_form" method="post" action="submit.php" onSubmit="return false">
            <?php print drupal_render($form['title']); ?>
            <?php print drupal_render($form['field_last_name']); ?>
            <?php print drupal_render($form['field_email']); ?>
            <?php print drupal_render($form['field_phone']); ?>
            <?php print drupal_render($form['field_date']); ?>
            <?php print drupal_render($form['field_gender']); ?>
            <?php print drupal_render($form['field_message']); ?>
            <?php print drupal_render($form['actions']); ?>
            <?php print render($form['form_build_id']); ?>
            <?php print render($form['form_token']); ?>
            <?php print render($form['form_id']); ?>
        </form>
    </div>
</div>