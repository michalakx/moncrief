<?php
/**
 * @file
 * bootstrap-panel.tpl.php
 *
 * Markup for Bootstrap panels ([collapsible] fieldsets).
 */

?>
<?php if ($prefix): ?>
  <?php print $prefix; ?>
<?php endif; ?>
<fieldset <?php print $attributes; ?> >
  <?php if ($title): ?>
    <?php if ($collapsible): ?>
      <legend class="panel-heading">
        <h4 class="panel-title<?php print (!$collapsed ? ' active' : ''); ?>">
          <a href="#<?php print $id; ?>" class="fieldset-legend" data-toggle="collapse">
            <span><i class="fa fa-plus<?php print (!$collapsed ? ' fa-minus' : ''); ?>"></i></span>
            <?php print $title; ?>
          </a>
        </h4>
      </legend>
    <?php else: ?>
      <legend class="panel-heading">
        <span class="panel-title fieldset-legend">
          <?php print $title; ?>
        </span>
      </legend>
    <?php endif; ?>
  <?php endif; ?>
  <?php if ($collapsible): ?>
    <div id="<?php print $id; ?>" class="panel-collapse collapse fade<?php print (!$collapsed ? ' in' : ''); ?>">
  <?php endif; ?>
  <div class="panel-body">
    <?php if ($description): ?>
      <p class="help-block">
        <?php print $description; ?>
      </p>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
  <?php if ($collapsible): ?>
    </div>
  <?php endif; ?>
</fieldset>
<?php if ($suffix): ?>
  <?php print $suffix; ?>
<?php endif; ?>
