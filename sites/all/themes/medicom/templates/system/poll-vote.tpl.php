<?php

/**
 * @file
 * Default theme implementation to display voting form for a poll.
 *
 * - $choice: The radio buttons for the choices in the poll.
 * - $title: The title of the poll.
 * - $block: True if this is being displayed as a block.
 * - $vote: The vote button
 * - $rest: Anything else in the form that may have been added via
 * - $nid: ID of content poll
 *   form_alter hooks.
 *
 * @see template_preprocess_poll_vote()
 *
 * @ingroup themeable
 */
$arrTitle = explode(' ', $title);
$i = 0;
foreach ($arrTitle as $key=>$value) {
  if ($i % 2 == 1) {
    $arrTitle[$key] = '<span>'.$value.'</span>';
  }
  $i++;
}
$title = implode(' ', $arrTitle);
?>
<div class="poll">
  <div class="vote-form">
    <div class="choices">
      <?php if ($block): ?>
        <h2 class="bordered light"><?php print $title; ?></h2>
      <?php endif; ?>
      <?php print $choice; ?>
    </div>
    <?php print $vote; ?>
  </div>
  <?php // This is the 'rest' of the form, in case items have been added. ?>
  <?php print $rest ?>
</div>
