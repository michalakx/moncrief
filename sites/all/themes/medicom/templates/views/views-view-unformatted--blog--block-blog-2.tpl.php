<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
	$col_left = '';
	$col_right = '';
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php $i = 0; foreach ($rows as $id => $row): ?>  
  <?php 
  	if ($i%2 == 0) {
  		$col_left .= '<article class="blog-item blog-full-width">'.$row.'</article>';
  	}
  	else {
  		$col_right .= '<article class="blog-item blog-full-width">'.$row.'</article>';
  	}
	?>  
<?php $i++; endforeach; ?>
<div class="row blog-wrapper">
	<div class="col-md-6 clearfix"><?php print $col_left; ?></div>
	<div class="col-md-6 clearfix"><?php print $col_right; ?></div>
</div>