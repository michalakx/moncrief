<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
	$col_left = '';
	$col_right = '';
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php $i = 0; foreach ($rows as $id => $row): ?>  
  <?php 
  		$col_ .= '<article class="blog-item">'.$row.'</article>';
	?>  
<?php $i++; endforeach; ?>
<div class="row">
	<div class="col-md-12"><?php print $col_; ?></div>
</div>