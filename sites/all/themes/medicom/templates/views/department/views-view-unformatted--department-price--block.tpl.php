<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
	$col_left = '';
	$col_right = '';
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php $i = 0; foreach ($rows as $id => $row): ?>  
  <?php 
  $li = (int)($i/2);
  	if ($i%2 == 0) {
      if($li%2 == 0){
        $col_left .= '<li class="list-dark">'.$row.'</li>';
      } else {
        $col_left .= '<li class="list-light">'.$row.'</li>';
      }
  	}
  	else {
      if($li%2 == 0){
        $col_right .= '<li class="list-dark">'.$row.'</li>';
      } else {
        $col_right .= '<li class="list-light">'.$row.'</li>';
      }
  	}
	?>  
<?php $i++; endforeach; ?>
<ul class="list-unstyled pricing-table first">
  <li class="table-heading">
    <span>Treatments</span>
      <span class="text-right">price</span>
  </li>
  <?php print $col_left; ?>
</ul>
<ul class="list-unstyled pricing-table">
  <li class="table-heading">
      <span>Treatments</span>
      <span class="text-right">price</span>
  </li>
  <?php print $col_right; ?>
</ul>