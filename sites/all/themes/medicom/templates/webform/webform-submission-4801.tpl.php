<?php

/**
 * @file
 * Customize the display of a webform submission.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The Webform submission array.
 * - $email: If sending this submission in an e-mail, the e-mail configuration
 *   options.
 * - $format: The format of the submission being printed, either "html" or
 *   "text".
 * - $renderable: The renderable submission array, used to print out individual
 *   parts of the submission, just like a $form array.
 */

$options = _extract_select_options ($node->webform['components'][20]['extra']['items']);
$options_mob = _extract_select_options ($node->webform['components'][21]['extra']['items']);
$options_cl = _extract_select_options ($node->webform['components'][23]['extra']['items']);
$options_gen = _extract_select_options ($node->webform['components'][27]['extra']['items']);
$options_age = _extract_select_options ($node->webform['components'][28]['extra']['items']);
$options_lan = _extract_select_options ($node->webform['components'][29]['extra']['items']);
$options_in = _extract_select_options ($node->webform['components'][31]['extra']['items']);
$options_ty = _extract_select_options ($node->webform['components'][32]['extra']['items']);

$chosen_mob = $options_mob[$submission->data[21][0]];
$chosen_cl = $options_cl[$submission->data[23][0]];

?>

 <section>
	<div class="row">
		<label>Today's Date</label>
		<p><?php print $submission->data[2][0]; ?></p>
	</div>
	<div class="row">
		<h3>Contact Information.</h3>
		<div class="item">
			<label>First Name</label>
			<p><?php print $submission->data[4][0]; ?></p>
		</div>
		<div class="item">
			<label>Last Name</label>
			<p><?php print $submission->data[5][0]; ?></p>
		</div>
		<div class="item">
			<label>Title</label>
			<p><?php print $submission->data[6][0]; ?></p>
		</div>
		<div class="item">
			<label>Organization</label>
			<p><?php print $submission->data[7][0]; ?></p>
		</div>
		<div class="item">
			<label>Phone Number</label>
			<p><?php print $submission->data[8][0]; ?></p>
		</div>
		<div class="item">
			<label>Email</label>
			<p><?php print $submission->data[9][0]; ?></p>
		</div>
	</div>
	<div class="row">
		<h3>Request for Moncrief Mobile Clinic</h3>
		<div class="item">
			<label>Requested Screening Date</label>
			<p><?php print $submission->data[12][0]; ?></p>
		</div>
		<div class="item">
			<label>Screening Location</label>
			<p><?php print $submission->data[13][0]; ?></p>
		</div>
		<div class="item">
			<label>Screening Address</label>
			<p><?php print $submission->data[14][0]; ?></p>
		</div>
		<div class="item">
			<label>City</label>
			<p><?php print $submission->data[15][0]; ?></p>
		</div>
		<div class="item">
			<label>State</label>
			<p><?php print $submission->data[16][0]; ?></p>
		</div>
		<div class="item">
			<label>Zip</label>
			<p><?php print $submission->data[18][0]; ?></p>
		</div>
		<div class="item">
			<label>Screening County</label>
			<p><?php print $submission->data[17][0]; ?></p>
		</div>
		<div class="item">
			<label>Screening type requested (select all that apply):</label>
			<?php
			foreach ($submission->data[20] as $key => $value) {
				print '<p>'.$options[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Is the mobile being requested in conjunction with other vendors (for example, at a health fair or community event)?</label>
			<p><?php print $chosen_mob; ?></p>
		</div>
		<div class="item">
			<label>Please list the event</label>
			<p><?php print $submission->data[22][0]; ?></p>
		</div>
		<div class="item">
			<label>Do you have the space for a 72-foot-long, 14-foot-high, 16-foot-wide mobile clinic (approximately 12 parking lot spaces) plus a 10-foot security parameter around the clinic site?</label>
			<p><?php print $chosen_cl; ?></p>
		</div>
	</div>
	<div class="row">
		<h3>Please provide general information about those who will be receiving services.</h3>
		<div class="item">
			<label>Gender (Select all that apply)</label>
			<?php
			foreach ($submission->data[27] as $key => $value) {
				print '<p>'.$options_gen[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Age Range (select all that apply)</label>
			<?php
			foreach ($submission->data[28] as $key => $value) {
				print '<p>'.$options_age[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Language</label>
			<?php
			foreach ($submission->data[29] as $key => $value) {
				print '<p>'.$options_lan[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Other Language</label>
			<p><?php print $submission->data[30][0]; ?></p>
		</div>
		<div class="item">
			<label>Insurance Status (select all that apply)</label>
			<?php
			foreach ($submission->data[31] as $key => $value) {
				print '<p>'.$options_in[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Insured Types</label>
			<?php
			foreach ($submission->data[32] as $key => $value) {
				print '<p>'.$options_ty[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Please list private insurance types.</label>
			<p><?php print $submission->data[33][0]; ?></p>
		</div>
		<div class="item">
			<label>Please provide any additional information you would like us to know about your request.</label>
			<p><?php print $submission->data[34][0]; ?></p>
		</div>
	</div>
</section>
	
