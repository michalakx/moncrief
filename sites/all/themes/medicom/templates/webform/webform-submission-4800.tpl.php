<?php

/**
 * @file
 * Customize the display of a webform submission.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The Webform submission array.
 * - $email: If sending this submission in an e-mail, the e-mail configuration
 *   options.
 * - $format: The format of the submission being printed, either "html" or
 *   "text".
 * - $renderable: The renderable submission array, used to print out individual
 *   parts of the submission, just like a $form array.
 */

$options = _extract_select_options ($node->webform['components'][15]['extra']['items']);
$options_ser = _extract_select_options ($node->webform['components'][11]['extra']['items']);
$options_type = _extract_select_options ($node->webform['components'][20]['extra']['items']);
$options_gen = _extract_select_options ($node->webform['components'][31]['extra']['items']);
$options_age = _extract_select_options ($node->webform['components'][32]['extra']['items']);
$options_lan = _extract_select_options ($node->webform['components'][34]['extra']['items']);
$options_mix = _extract_select_options ($node->webform['components'][37]['extra']['items']);
$options_pay = _extract_select_options ($node->webform['components'][38]['extra']['items']);
$chosen_in = $options[$submission->data[15][0]];
$chosen_type = $options_type[$submission->data[20][0]];
?>

<section>
	<div class="row">
		<label>Today's Date</label>
		<p><?php print $submission->data[2][0]; ?></p>
	</div>
	<div class="row">
		<h3>Please provide general information about the event.</h3>
		<div class="item">
			<label>Name of event</label>
			<p><?php print $submission->data[4][0]; ?></p>
		</div>
		<div class="item">
			<label>Date of Event</label>
			<p><?php print $submission->data[5][0]; ?></p>
		</div>
		<div class="item">
			<label>Start Time of Event</label>
			<p><?php print $submission->data[6][0]; ?></p>
		</div>
		<div class="item">
			<label>End Time of Event</label>
			<p><?php print $submission->data[7][0]; ?></p>
		</div>
		<div class="item">
			<label>EVENT ADDRESS</label>
			<p><?php print $submission->data[50][0]; ?></p>
			<p><?php print $submission->data[51][0]; ?></p>
			<p><?php print $submission->data[52][0]; ?></p>
			<p><?php print $submission->data[53][0]; ?></p>
		</div>
		<div class="item">
			<label>How are you promoting this event?</label>
			<p><?php print $submission->data[14][0]; ?></p>
		</div>	
		<div class="item">
			<label>Expected Attendance</label>
			<p><?php print $submission->data[47][0]; ?></p>
		</div>
		<div class="item">
			<label>Is this event indoors or outdoors?</label>
			<p><?php print $chosen_in; ?></p>
		</div>
		<div class="item">
			<label>How many years has this event occurred?</label>
			<p><?php print $submission->data[42][0]; ?></p>
		</div>
		<div class="item">
			<label>Last year's attendance</label>
			<p><?php print $submission->data[43][0]; ?></p>
		</div>
		<div class="row">
			<h3>Services Requested</h3>
		<div class="item">
			<label>Services</label>
			<p>Please note: If you requesting the Moncrief Mobile Clinic, please utilize the Request for Mobile Clinic form.</p>
			<?php
			foreach ($submission->data[11] as $key => $value) {
				print '<p>'.$options_ser[$value].'</p>';
			}
			?>
		</div>
		<?php
		if ($submission->data[11][1] == 2) { ?>
			<div class="item">
			<label>Topic of Interest</label>
			<p><?php print $submission->data[12][0]; ?></p>
			</div>
		<?php } ?>
		</div>
		<div class="row">
		<div class="item">
			<h3>Please provide your contact information</h3>
			<label>Name of Requesting Organization</label>
			<p><?php print $submission->data[19][0]; ?></p>
		</div>
		<div class="item">
			<label>Type of Organization</label>
			<p><?php print $chosen_type; ?></p>
		</div>
		<div class="item">
			<label>If "Other" please specify</label>
			<p><?php print $submission->data[22][0]; ?></p>
		</div>
		<div class="item">
			<h4>Primary Contact</h4>
			<label>First Name</label>
			<p><?php print $submission->data[24][0]; ?></p>
			<label>Last Name</label>
			<p><?php print $submission->data[25][0]; ?></p>
			<label>Title</label>
			<p><?php print $submission->data[26][0]; ?></p>
			<label>Telephone Number</label>
			<p><?php print $submission->data[27][0]; ?></p>
			<label>Email Address</label>
			<p><?php print $submission->data[28][0]; ?></p>
		</div>
		</div>
		<div class="row">
			<h4>Please provide general information about those who will be receiving services</h4>
		<div class="item">
			<label>Gender</label>
			<?php
			foreach ($submission->data[31] as $key => $value) {
				print '<p>'.$options_gen[$value].'</p>';
			}
			?>
			<label>Age Range</label>
			<?php
			foreach ($submission->data[32] as $key => $value) {
				print '<p>'.$options_age[$value].'</p>';
			}
			?>
			<label>Language</label>
			<?php
			foreach ($submission->data[34] as $key => $value) {
				print '<p>'.$options_lan[$value].'</p>';
			}
			?>
			<label>If "Other" please specify</label>
			<p><?php print $submission->data[35][0]; ?></p>
			<label>Payer mix</label>
			<?php
			foreach ($submission->data[37] as $key => $value) {
				print '<p>'.$options_mix[$value].'</p>';
			}
			?>
			<label>Insured Payer Mix</label>
			<?php
			foreach ($submission->data[38] as $key => $value) {
				print '<p>'.$options_pay[$value].'</p>';
			}
			?>
		</div>
		<div class="item">
			<label>Please provide any additional information you would like us to know about this event.</label>
			<p><?php print $submission->data[40][0]; ?></p>
		</div>
	</div>
	</div>
</section>
	
