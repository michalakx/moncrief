<?php

/**
 * @file
 * Bootstrap sub-theme.
 *
 * Place your custom PHP code in this file.
 */

/**
 * Implements hook_pager()
 */
function moncrief_pager(array $variables) {
  $output = "";
  $items = array();
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];

  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // Current is the page we are currently paged to.
  $pager_current = $pager_page_array[$element] + 1;
  // First is the first page listed by this pager piece (re quantity).
  $pager_first = $pager_current - $pager_middle + 1;
  // Last is the last page listed by this pager piece (re quantity).
  $pager_last = $pager_current + $quantity - $pager_middle;
  // Max is the maximum page number.
  $pager_max = $pager_total[$element];

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  //$element = array('class' => array('pagination-right'));
  // End of generation loop preparation.
  /* @noinspection PhpUnhandledExceptionInspection */
  $li_first = theme('pager_first', array(
    'text' => (isset($tags[0]) ? $tags[0] : t('first')),
    'element' => $element,
    'parameters' => $parameters,
    'attributes' => 'test-first'
  ));
  /* @noinspection PhpUnhandledExceptionInspection */
  $li_previous = theme('pager_previous', array(
    'text' => (isset($tags[1]) ? $tags[1] : t('previous')),
    'element' => $element,
    'interval' => 1,
    'parameters' => $parameters,
  ));
  /* @noinspection PhpUnhandledExceptionInspection */
  $li_next = theme('pager_next', array(
    'text' => (isset($tags[3]) ? $tags[3] : t('next')),
    'element' => $element,
    'interval' => 1,
    'parameters' => $parameters,
    'attributes' => array('class' => 'test-nx')
  ));
  /* @noinspection PhpUnhandledExceptionInspection */
  $li_last = theme('pager_last', array(
    'text' => (isset($tags[4]) ? $tags[4] : t('last')),
    'element' => $element,
    'parameters' => $parameters,
    'attributes' => 'test-last'
  ));
  if ($pager_total[$element] > 1) {

    // Only show "first" link if set on components' theme setting.
    if ($li_first && bootstrap_setting('pager_first_and_last')) {
      $items[] = array(
        'class' => array('pager-first page-item'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('prev page-item'),
        'data' => $li_previous,
      );
    }
    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      // if ($i > 1) {
      //   $items[] = array(
      //     'class' => array('pager-ellipsis', 'disabled'),
      //     'data' => '<span>…</span>',
      //   );
      // }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          /* @noinspection PhpUnhandledExceptionInspection */
          $items[] = array(
            // 'class' => array('pager-item'),.
            'data' => theme('pager_previous', array(
              'text' => $i,
              'element' => $element,
              'interval' => ($pager_current - $i),
              'parameters' => $parameters,
            )),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            // Add the active class.
            'class' => array('page-item active'),
            'data' => "<span class='page-link'>$i</span>",
          );
        }
        if ($i > $pager_current) {
          /* @noinspection PhpUnhandledExceptionInspection */
          $items[] = array(
            'class' => array('page-item'),
            'data' => theme('pager_next', array(
              'text' => $i,
              'element' => $element,
              'interval' => ($i - $pager_current),
              'parameters' => $parameters,
            )),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          //  'class' => array('pager-ellipsis', 'disabled'),
          //  'data' => '<span>…</span>',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('next page-item'),
        'data' => $li_next,
      );
    }
    // // Only show "last" link if set on components' theme setting.
    if ($li_last && bootstrap_setting('pager_first_and_last')) {
      $items[] = array(
        'class' => array('pager-last page-item'),
        'data' => $li_last,
      );
    }

    $build = array(
      '#theme_wrappers' => array('container__pager'),
      '#attributes' => array(
        'class' => array(
          'text-center',
        ),
      ),
      'pager' => array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => array('pagination'),
        ),
      ),
    );

    return drupal_render($build);
  }
  return $output;
}

/**
 * Implements hook_pager_link()
 */
function moncrief_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }
  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array(
        '@number' => $text,
      ));
    }
  }
  $attributes['href'] = url($_GET['q'], array(
    'query' => $query,

  ));
  $attributes['class'] = 'page-link';
  return '<a' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}

function moncrief_menu_link(array $variables) {
$element = $variables ['element'];
$sub_menu = '';

if ($element ['#below']) {
$sub_menu = drupal_render($element ['#below']);
}
$output = l($element ['#title'], $element ['#href'], $element ['#localized_options']);
return '<li' . drupal_attributes($element ['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function moncrief_preprocess_page(&$vars) {
 	if (isset($vars['node'])) {
		$vars['theme_hook_suggestion'] = 'page__'.$vars['node']->type; //
	}
}

function moncrief_preprocess_entity(&$variables) {
    if ($variables['entity_type'] == 'paragraphs_item') {
        if(!empty($variables['field_white_bg'][0]['value'])){
            if($variables['field_white_bg'][0]['value'] == 1){
                $variables['classes_array'][]  = 'bg-white';
            }
        }
        
        if(!empty($variables['field_gray_bg'][0]['value'])){
            if($variables['field_gray_bg'][0]['value'] == 1){
                $variables['classes_array'][]  = 'bg-gray preventive-care-section';
            }
        }
    }
}