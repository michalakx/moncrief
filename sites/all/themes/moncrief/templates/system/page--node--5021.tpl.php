<div class="page-container">
  <?php if (!empty($page['top_bar'])): ?>
  <div class="top-bar">
    <div class="container">
      <?php print render($page['top_bar']); ?>
    </div>
  </div>
  <?php endif; ?>
  <header class="header shadow-sm">
    <div class="main-navbar">
      <div class="container">
        <div class="menu-inner">
          <?php if ($logo): ?>
            <div class="navbar-logo">
              <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
              </a>
            </div>
            <?php endif; ?>
            <div class="navbar-menu">
              <?php if (!empty($page['menu_top'])): ?>
                <div class="menu-top">
                  <?php print render($page['menu_top']); ?>
                </div>
              <?php endif; ?>
              <?php if (!empty($page['menu'])): ?>
                <div class="menu-section">
                  <?php print render($page['menu']); ?>
                </div>
              <?php endif; ?>
            </div>
            <div class="donate-block-block">
              <p><a class="feedback" href="https://engage.utsouthwestern.edu/donate_MCI" rel="noopener noreferrer" target="_blank">Donate Now </a></p>
            </div>
            <div class="hamburger">
              <div class="hamburger-bar">
                  <span></span>
              </div>
            </div>
            <div class="responsive-menu-wraper">
              <div class="responsive-menu-holder"></div>
              <?php if (!empty($page['menu_top'])): ?>
                <div class="menu-top">
                  <?php print render($page['menu_top']); ?>
                </div>
              <?php endif; ?>
            </div>
        </div>
      </div>
    </div>
  </header>

  <div class="main-content-wrapper">

    <?php if (!empty($page['slider'])): ?>
    <section class="banner-section">
      <?php print render($page['slider']); ?>
    </section>
    <?php endif; ?>

    <?php if (!empty($page['map'])): ?>
    <section class="map-section">
      <?php print render($page['map']); ?>
    </section>
    <?php endif; ?>
    <section class="contact-main-content">
      <div class="container">
        <div class="row">
          <?php if (!empty($page['content'])): ?>
            <div class="col-md-8 contact-form-col">
                <?php print render($page['content']); ?>
            </div>
          <?php endif; ?>
            <div class="col-md-4 contact-detail-col">
          <?php if (!empty($page['content_bottom'])): ?>
              <?php print render($page['content_bottom']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </section>


    <?php if (!empty($page['latest_news'])): ?>
      <section class="latest-news-section">
      <div class="container">
        <?php print render($page['latest_news']); ?>
      </div>
      </section>
    <?php endif; ?>

  </div>
    <?php if (!empty($page['footer_top'])): ?>
  <div class="footer-top">
    <?php print render($page['footer_top']); ?>
  </div>
    <?php endif; ?>

  <footer class="footer-wrapper">
    <div class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4 footer-left-section pl-xl-0 py-5">
            <?php print render($page['footer_1']); ?>
          </div>
          <div class="col-md-8 row bg-white">
            <div class="col-sm-8 pl-4">
              <?php print render($page['footer_2']); ?>
            </div>
            <div class="col-sm-4 pl-4">
              <?php print render($page['footer_3']); ?>
            </div>
          <?php if (!empty($page['footer_4'])): ?>
              <hr />
              <div class="hours pl-4">
                <?php print render($page['footer_4']); ?>
              </div>
           <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="copy-right">
      <div class="container">
          <?php print render($page['copyright']); ?>
      </div>
    </div>
  </footer>
  
</div>





