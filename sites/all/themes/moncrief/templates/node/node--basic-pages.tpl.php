<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="basic-details-wrapper"> 
    <?php if (!empty($content['field_left_title_right_descripti'])): ?>
    <div class="left-title-right-desc">
        <div class="container">
             <?php print render($content['field_left_title_right_descripti']);?>     
        </div>
    </div>
    <?php endif; ?>
    
    <?php if (!empty($content['body'])): ?>
  	<div class="title-text-section">
  		<div class="container">
    		<div class="content-wrapper"><?php print render($content['body']);?></div>
  		</div>
  	</div>
    <?php endif; ?>

    <?php if (!empty($content['field_text_image'])): ?>
    <div class="full-section">
  			<?php print render($content['field_text_image']);?>
    </div>
    <?php endif; ?>

    <?php if (!empty($content['field_text_image_container'])): ?>
    <div class="container-section">
        <?php print render($content['field_text_image_container']);?>
    </div>
    <?php endif; ?>

    <?php if (!empty($content['field_map_image'])): ?>
    <div class="title-image-center">
    	<div class="container">
    		<h2 class="title-type-1"><?php print render($content['field_map_title']);?></h2>
        <?php print render($content['field_map_image']);?>
    	</div>
    </div>
    <?php endif; ?>

    <?php if(!empty($content['field_slider_content'])): ?>
    <div class="slider-paragraph-section py-4">
      <div class="container">
        <div class="flex-md-row">
          <div class="slider-section col-md-6">
            <?php print views_embed_view('left_image_paragrah_swiper', 'block_1');?> 
          </div>
          <div class="right-text pt-smd-3 col-md-6">
            <?php print render($content['field_slider_content']);?>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if (!empty($content['field_left_image'])): ?>
      <div class="about-us-wrapper">
        <div class="container">
          <div class="left-image-content row align-items-center">
            <?php if (!empty($content['field_left_image'])): ?>
            <div class="left-image col-md-6 w-auto pull-left">
                <?php print render($content['field_left_image']);?>
            </div>
            <?php endif; ?> 

            <?php if (!empty($content['field_left_image_title'])): ?>
            <div class="image-title col-md-6 px-15">
                <?php print render($content['field_left_image_title']);?>
            </div>
            <?php endif; ?>

            <?php if (!empty($content['field_left_image_description'])): ?>
            <div class="image-desc col-md-12 px-15">
                <?php print render($content['field_left_image_description']);?>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  <!--   <?php //if (!empty($content['field_accordian_content'])): ?>
    <div class="accordian-secction">
      <div class="container">
      <div class="accordian-title"><?php // print render($content['field_accordian_content']);?></div>     
    </div>
    </div>
  <?php //endif; ?> -->
    <?php if (!empty($content['field_video_section_title'])): ?>
      <div class="video-section">
        <div class="container">

        <div class="video-section-title"><?php print render($content['field_video_section_title']);?>          
        </div> 
        <div class="video-section-paragraph">
          <?php print render($content['field_video_section']);?>
        </div>    
      </div>
      </div>
    <?php endif; ?>
    
    <?php if (!empty($content['field_paragraphs'])): ?>
    <div class="cus-paragraphs">
        <?php print render($content['field_paragraphs']);?>
    </div>
  <?php endif; ?>
  </div>

        
</article>
