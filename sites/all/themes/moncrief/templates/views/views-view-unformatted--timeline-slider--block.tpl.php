<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
 <div class="swiper-container swiper-slider-timeline">
 <div class="swiper-wrapper">
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?> swiper-slide"<?php endif; ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>
	<div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>