<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$block = module_invoke('webform', 'block_view', 'client-block-4979');

//$block_request = module_invoke('webform', 'block_view', 'appointment_form1');
?>

<div class="request_appointment pt-3">
	<div class="container">
        <div class="row flex-md-row flex-reverse">
            <div class="col-md-6 appointment-form">
            	<h3 class="theme-color">Request an Appointment</h3>
                <div class="disc"><p>Please fill out the information below in order to request an appointment.  A member of our staff will be in contact shortly.</p></div>
				<?php print render($block['content']); ?>
            </div>
        	<div class="col-md-6">
                <div class="center-img">
				    <?php print $output; ?>
                </div>
            </div>
        </div>
    </div>
</div>