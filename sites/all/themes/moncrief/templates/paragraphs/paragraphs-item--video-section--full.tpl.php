<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
  	<div class="video-section-items">
  		<div class="video-section-inner">
        <div class="video-wrape">
         <?php
         // print_r($content);
         $video_id = $content['field_youtube_video']['#object']->field_youtube_video[LANGUAGE_NONE][0]['value'];
         ?>
           <iframe width="560" height="315" src="<?php print $video_id; ?>?autoplay=0&mute=1&loop=1&controls=0&showinfo=0&playlist=<?php print $video_id; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
	    </div>
      <div class="video-desc">
        <?php print render($content['field_overview_description']); ?>
      </div>
  	</div>
  </div>
</div>
