<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> pt-3 pb-3"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <div class="container in-front">
      <div class="row">
         <?php
      if(!empty($content['field_image'])): ?>
      <div class="col-md-6">
          <?php print render($content['field_description']); ?>
      </div>
      <?php else: ?>
        <div class="col-md-12">
          <?php print render($content['field_description']); ?>
        </div>
        <?php endif; ?>
      </div>
    </div>

    <?php if(!empty($content['field_image'])): ?>
      <?php
      if(!empty($content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value']) && ($content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'] == '392x600' || $content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'] == 'no_crop' || $content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'] == '575x290')): ?>
      <div class="image-right">
        <div class="container">
        <?php
          $crop_img = $content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'];
          $bg_img = image_style_url('392x600', $content['field_description']['#object']->field_image[LANGUAGE_NONE][0]['uri']);

          if($content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'] == 'no_crop'){
          $bg_img = file_create_url($content['field_description']['#object']->field_image[LANGUAGE_NONE][0]['uri']);
          }

          if($content['field_description']['#object']->field_image_crop[LANGUAGE_NONE][0]['value'] == '575x290'){
          $bg_img = image_style_url('575x290', $content['field_description']['#object']->field_image[LANGUAGE_NONE][0]['uri']);
          }

        if (!empty($content['field_description']['#object']->field_url[LANGUAGE_NONE][0]['url'])):
          $bg_url = $content['field_description']['#object']->field_url[LANGUAGE_NONE][0]['url'];
        ?>
        <a href="/<?php print $bg_url?>" style="display:block;">
          <div class="half-col-right" style="background-image: url('<?php print $bg_img;?>');  background-size: contain;"></div>
        </a>
        <?php else: ?>
        <div class="half-col-right" style="background-image: url('<?php print $bg_img;?>');  background-size: contain; background-position: left;">
        <?php endif; ?>

        </div>
      </div></div>
    <?php else: ?>
    <div class="image-right">
        <?php
        $bg_img = '';
        if (!empty($content['field_description']['#object']->field_image[LANGUAGE_NONE][0]['uri'])) {
          $bg_img = file_create_url($content['field_description']['#object']->field_image[LANGUAGE_NONE][0]['uri']);
        }

        if (!empty($content['field_description']['#object']->field_url[LANGUAGE_NONE][0]['url'])):
          $bg_url = $content['field_description']['#object']->field_url[LANGUAGE_NONE][0]['url'];
        ?>
        <a href="/<?php print $bg_url?>" style="display:block;"><div class="half-col-right" style="background-image: url('<?php print $bg_img;?>');  background-size: contain;"></div>
        </a>
        <?php else: ?>
        <div class="half-col-right" style="background-image: url('<?php print $bg_img;?>');  background-size: contain;">
        <?php endif; ?>

        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>
  </div>
</div>
