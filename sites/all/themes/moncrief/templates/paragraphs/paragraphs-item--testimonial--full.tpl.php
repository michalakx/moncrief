<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content row flex-sm-row flex-reverse"<?php print $content_attributes; ?>>
    <div class="col-sm-5 testimonial-img">
       <?php print render($content['field_user_image']); ?>
    </div>
  	<div class="col-sm-7 testimonial-content">
      <div class="testimonial-quote">
     <!-- <img src="sites/all/themes/moncrief/images/quote.svg" class="img-fluid mr-3" width="25px;">  -->
      <?php print render($content['field_blue_text']); ?>
</div>
      <div class="testimonial-normal">
      <!--<h6 class="font-13 mb-0">Story By</h6>-->
      <h5 class="author-name">
        <?php print render($content['field_story_by'][0]).', '. $content['field_as_a']['#items'][0]['value']; ?>
        <!--<small class="text-muted">(<?php print render($content['field_as_a']['#items'][0]['value']); ?>)</small> -->
      </h5>
      <?php print render($content['field_normal_text']); ?>
      </div>
    </div>
  </div>
</div>
