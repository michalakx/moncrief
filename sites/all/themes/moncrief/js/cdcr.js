(function ($, Drupal ) {

  $(document).on("click", ".popover .close" , function(e){
    e.preventDefault();
    $(this).parents(".popover").popover('hide');
  });


  $(window).on('load', function() {
    if($('.webform-client-form .captcha').length > 0) {
        $('.webform-client-form .form-actions .webform-submit.button-primary.form-submit').attr('disabled', 'disabled')
    }
  });
  
  var captchaResponse = function captchaResponse() {
    $('.webform-client-form .form-actions .webform-submit.button-primary.form-submit').attr('disabled', false);
  };  
  window.captchaResponse = captchaResponse;

  var captchaExpiredResponse = function captchaExpiredResponse() {
    $('.webform-client-form .form-actions .webform-submit.button-primary.form-submit').attr('disabled', 'disabled');
  };  
  window.captchaExpiredResponse = captchaExpiredResponse;
  
	$(document).ready(function() {
	 
    $('[data-toggle="popover"]').popover();

    $("select#edit-submitted-new-1599430349066 > option:first").text("Select Appointment Type");
    $("select#edit-submitted-select-county > option:first").text("Select County");
    
    //$('.collapse').collapse('hide');
    $('.button-work-place button').on('click', function(){
      var target = $(this).data('target');
      //$('.in').removeClass('in');
      //$(target).addClass('in');

    });
    $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".main-navbar").addClass("fixed");
    } else {
        $(".main-navbar").removeClass("fixed");
    }
});

    $('#payroll-deduction').trigger("click");
    $('#payroll-deduction').parent().tab("show");

$( "#p2, #p3, #p4, #p5, #p6" ).click( function() {
$('#payroll-deduction').parent().removeClass("active");    
} ); 
    $('.navbar-menu nav.nav-menu').meanmenu({
      meanScreenWidth: "991",
      meanMenuContainer: '.responsive-menu-holder'
    });
    $('.hamburger').on('click', function(){
      $('.meanmenu-reveal').click();
      $('body').toggleClass('menu-open');
      $('.responsive-menu-wraper').toggle('slow');
    });
    $('#video-gallery').lightGallery(); 
		var swiper4 = new Swiper('.swiper-patient', {
              slidesPerView: 1,
              spaceBetween: 20,
              speed: 2000,
              pagination: {
                 el: '.swiper-pagination',
                 clickable: true,
              },
              autoplay: {
                 delay: 7000,
               disableOnInteraction: false
              },
              breakpoints: {
                640: {
                  slidesPerView: 2,
                },
                991: {
                  slidesPerView: 3,
                },
                1199: {
                  slidesPerView: 4,
                }                        
              },
          });  

    var swiper1 = new Swiper('.swiper-slider', {
              speed: 2000,
              pagination: {
                 el: '.swiper-pagination',
                 clickable: true,
              },
               autoplay: {
                   delay: 7000,
                 disableOnInteraction: false
                },
          });
    var swiper2 = new Swiper('.swiper-testimonial', {
              pagination: {
                 el: '.swiper-pagination',
                 clickable: true,
              },
               autoplay: {
                   delay: 7000,
                 disableOnInteraction: false
                },
          });
          var swiper3 = new Swiper('.swiper-mobile-outreach', {
              speed: 1000,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
          });
          var swiper4 = new Swiper('.swiper-slider-timeline', {
              speed: 1000,
/* 
 	autoplay: {
                   delay: 5000,
                 disableOnInteraction: false
                },
*/
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
          });

          $('.slide-nav li p').on('click', function(){
             $('.slide-nav li').removeClass('active');
              $(this).parent().addClass('active');
              var id = $(this).attr("id").split('_')[1];
              swiper4.slideTo(id);
          });




	});

}(jQuery, Drupal));



